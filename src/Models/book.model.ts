import Mongoose, { Document, Schema } from 'mongoose'

export interface IBook extends Document {
  authors: string[]
  categories: string[]
  publisher: string
  rating: number
  title: string
  createdAt: Date
  updatedAt: Date
}

const bookSchema = new Schema(
  {
    authors: { type: [String], required: true, minlength: 1 },
    categories: { type: [String], required: false, default: [] },
    publisher: { type: String, required: true },
    rating: { type: Number, required: false, default: 0 },
    title: { type: String, required: true }
  },
  { timestamps: true }
)

const bookModel = Mongoose.model<IBook>('Book', bookSchema, 'books')

export default bookModel
