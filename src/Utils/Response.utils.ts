import { Response } from 'express'

type IResponsePayload = Record<string, any> | Array<Record<string, any>>

const handleError = (
  res: Response,
  status: number,
  error: string | Record<string, any>
) => {
  if (typeof error === 'string') res.status(status).json({ error })
  return res.status(status).json(error)
}

// ==> 200
const success = (res: Response, payload: IResponsePayload) =>
  res.status(200).json(payload)

const created = (res: Response, payload: IResponsePayload) =>
  res.status(201).json(payload)

// ==> 400
const badRequest = (res: Response, payload: string | IResponsePayload) =>
  handleError(res, 400, payload)

const unauthorized = (res: Response, payload: string | IResponsePayload) =>
  handleError(res, 401, payload)

const forbidden = (res: Response, payload: string | IResponsePayload) =>
  handleError(res, 403, payload)

const notFound = (res: Response, payload: string | IResponsePayload) =>
  handleError(res, 404, payload)

// ==> 500
const internalError = (res: Response, payload: IResponsePayload) =>
  handleError(res, 500, payload)

export default {
  // ==> 200
  success,
  created,

  // ==> 400
  badRequest,
  forbidden,
  notFound,
  unauthorized,

  // ==> 500
  internalError
}
