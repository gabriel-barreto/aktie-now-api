import Mongoose, { Document, Model } from 'mongoose'
import { Request, Response } from 'express'

import * as Yup from 'yup'
import { Books } from '../../../Controllers'
import { IBook } from '../../../Models/book.model'

describe('Books Controller', () => {
  const ModelMock: Partial<Model<Document>> = {
    find: jest.fn().mockReturnValue([]),
    findOne: jest.fn().mockReturnValue({ lean: jest.fn().mockReturnValue({}) }),
    findOneAndUpdate: jest.fn().mockReturnValue({}),
    findOneAndRemove: jest.fn().mockReturnValue({}),
    create: jest.fn().mockReturnValue({})
  }

  const ReqMock = { params: {}, body: {} }
  const ResMock: Partial<Response> = {
    status: jest.fn().mockReturnValue({ json: jest.fn() })
  }

  const SchemaMock = Yup.object().shape({})
  SchemaMock.isValid = jest.fn().mockReturnValue(true)

  const sut = Books({ Model: ModelMock as Model<IBook>, schema: SchemaMock })

  describe('List - Return a list of books', () => {
    it('should call Model.find to get all registered books', async () => {
      await sut.list(ReqMock as Request, ResMock as Response)
      expect(ModelMock.find).toHaveBeenCalled()
    })
  })

  describe('Get - Return a single book', () => {
    it('should call Model.findOne to get a single book', async () => {
      await sut.get(ReqMock as Request, ResMock as Response)
      expect(ModelMock.findOne).toHaveBeenCalled()
    })

    it('should call Model.findOne with bookId request param', async () => {
      const fakeBookId = Mongoose.Types.ObjectId()
      Object.assign(ReqMock.params, { bookId: fakeBookId })

      await sut.get(ReqMock as Request, ResMock as Response)
      expect(ModelMock.findOne).toHaveBeenCalledWith({ _id: fakeBookId })
    })
  })

  describe('Create - Register a new book', () => {
    it('should validate request body', async () => {
      await sut.create(ReqMock as Request, ResMock as Response)
      expect(SchemaMock.isValid).toHaveBeenCalled()
    })

    it("shouldn't call Model.create when payload is invalid", async () => {
      SchemaMock.isValid = jest.fn().mockReturnValue(false)
      await sut.create(ReqMock as Request, ResMock as Response)
      expect(ModelMock.create).not.toHaveBeenCalled()
    })

    it('should call Model.create when payload is valid', async () => {
      SchemaMock.isValid = jest.fn().mockReturnValue(true)
      await sut.create(ReqMock as Request, ResMock as Response)
      expect(ModelMock.create).toHaveBeenCalled()
    })

    it('should call Model.create with req.body without transform', async () => {
      const fakeBody = { foo: 'bar' }
      Object.assign(ReqMock.body, fakeBody)
      await sut.create(ReqMock as Request, ResMock as Response)
      expect(ModelMock.create).toHaveBeenCalledWith(fakeBody)
    })
  })

  describe('Update - Update a book infos', () => {
    it('should validate request body', async () => {
      await sut.create(ReqMock as Request, ResMock as Response)
      expect(SchemaMock.isValid).toHaveBeenCalled()
    })

    it('should call Model.findOne to check if Req Book ID exists', async () => {
      const fakeBookId = Mongoose.Types.ObjectId()
      Object.assign(ReqMock.params, { bookId: fakeBookId })
      await sut.update(ReqMock as Request, ResMock as Response)
      expect(ModelMock.findOne).toHaveBeenCalledWith({ _id: fakeBookId })
    })

    it("shouldn't call Model.findOneAndUpdate when payload is invalid", async () => {
      SchemaMock.isValid = jest.fn().mockReturnValue(false)

      await sut.update(ReqMock as Request, ResMock as Response)
      expect(ModelMock.findOneAndUpdate).not.toHaveBeenCalled()
    })

    it("shouldn't call Model.findOne when book ID wasn't found", async () => {
      ModelMock.findOne = jest.fn().mockReturnValue(null)

      await sut.update(ReqMock as Request, ResMock as Response)
      expect(ModelMock.findOne).not.toHaveBeenCalled()
    })

    it('should call Model.findOneAndUpdate when payload is valid', async () => {
      ModelMock.findOne = jest.fn().mockReturnValue({})
      SchemaMock.isValid = jest.fn().mockReturnValue(true)

      await sut.update(ReqMock as Request, ResMock as Response)
      expect(ModelMock.findOneAndUpdate).toHaveBeenCalled()
    })

    it('should call Model.findOneAndUpdate with bookId req param', async () => {
      const fakeBookId = Mongoose.Types.ObjectId()
      Object.assign(ReqMock.params, { bookId: fakeBookId })
      await sut.update(ReqMock as Request, ResMock as Response)

      expect(ModelMock.findOneAndUpdate).toHaveBeenCalledWith(
        { _id: fakeBookId },
        expect.any(Object),
        expect.any(Object)
      )
    })

    it('should call Model.findOneAndUpdate with req.body without transform', async () => {
      const fakeBody = { foo: 'bar' }
      Object.assign(ReqMock.body, fakeBody)
      await sut.update(ReqMock as Request, ResMock as Response)
      expect(ModelMock.findOneAndUpdate).toHaveBeenCalledWith(
        expect.any(Object),
        { $set: fakeBody },
        expect.any(Object)
      )
    })
  })

  describe('Remove - Remove a book', () => {
    it('should call Model.findOne to check if book ID exists', async () => {
      await sut.remove(ReqMock as Request, ResMock as Response)
      expect(ModelMock.findOne).toHaveBeenCalled()
    })

    it("shouldn't call Model.findOneAndRemove if book ID not exists", async () => {
      ModelMock.findOne = jest.fn().mockReturnValue(null)
      await sut.remove(ReqMock as Request, ResMock as Response)
      expect(ModelMock.findOneAndRemove).not.toHaveBeenCalled()
    })

    it('should call Model.findOneAndRemove', async () => {
      ModelMock.findOne = jest.fn().mockReturnValue({})
      await sut.remove(ReqMock as Request, ResMock as Response)
      expect(ModelMock.findOneAndRemove).toHaveBeenCalled()
    })

    it('should call Model.findOneAndRemove with bookId route param', async () => {
      const fakeBookId = Mongoose.Types.ObjectId()
      Object.assign(ReqMock.params, { bookId: fakeBookId })
      await sut.remove(ReqMock as Request, ResMock as Response)
      expect(ModelMock.findOneAndRemove).toHaveBeenCalledWith({
        _id: fakeBookId
      })
    })
  })
})
