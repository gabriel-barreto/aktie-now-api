import faker from 'faker'
import Mongoose from 'mongoose'
import { MongoMemoryServer } from 'mongodb-memory-server'
import request from 'supertest'

import App from '../../app'

interface IBookFactory {
  authors?: string[]
  categories?: string[]
  rating?: number
  publisher?: string
  title?: string
}
function bookFactory({
  authors = [faker.name.findName()],
  categories = [faker.lorem.word()],
  publisher = faker.lorem.words(2),
  rating = faker.random.number({ min: 0, max: 5 }),
  title = faker.lorem.words(2)
}: IBookFactory = {}) {
  return { authors, categories, publisher, rating, title }
}

const mongooseConnectionMock = () => null
const $http = request(App(mongooseConnectionMock).app)

describe('Books [/api/books]', () => {
  const endpoint = '/api/books'
  const mongod = new MongoMemoryServer({
    instance: { dbName: 'jest', port: 27019 }
  })

  beforeAll(async () => {
    const uri = await mongod.getUri()
    await Mongoose.connect(uri, {
      useCreateIndex: true,
      useFindAndModify: false,
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
  })

  afterAll(async () => {
    await Promise.all([Mongoose.disconnect(), mongod.stop()])
  })

  describe(`GET ${endpoint} - List all books`, () => {
    it('should return 404 when not found any book', async () => {
      const { status, body } = await $http.get(endpoint)

      expect(status).toBe(404)
      expect(body).toHaveProperty('error')
    })

    it('should return 200 when found any book', async () => {
      await Mongoose.model('Book').create(bookFactory())

      const { status, body } = await $http.get(endpoint)

      expect(status).toBe(200)
      expect(Array.isArray(body)).toBeTruthy()
      expect(body.length > 0).toBeTruthy()
    })

    it('should return a list of found books', async () => {
      const { _id: bookId } = await Mongoose.model('Book').create(bookFactory())
      const { body } = await $http.get(endpoint)

      expect(Array.isArray(body)).toBeTruthy()
      expect(body).toEqual(
        expect.arrayContaining([
          expect.objectContaining({ _id: bookId.toString() })
        ])
      )
    })
  })

  describe(`GET ${endpoint}/:bookId - Get a single book`, () => {
    let bookId = ''
    const book = bookFactory()

    beforeAll(async () => {
      const { _id } = await Mongoose.model('Book').create(book)
      bookId = _id.toString()
    })

    it('should return 404 when book with provided ID not found', async () => {
      const fakeId = Mongoose.Types.ObjectId()
      const { status, body } = await $http.get(`${endpoint}/${fakeId}`)

      expect(status).toBe(404)
      expect(body).toHaveProperty('error')
    })

    it('should return 200 when book with provided ID was found', async () => {
      const { status, body } = await $http.get(`${endpoint}/${bookId}`)
      expect(status).toBe(200)
      expect(body).toEqual(expect.objectContaining({ _id: bookId, ...book }))
    })
  })

  describe(`POST ${endpoint} - Register a new book`, () => {
    it('should return 400 when required prop was absent', async () => {
      const { status, body } = await $http.post(endpoint).send({})
      expect(status).toBe(400)
      expect(body).toHaveProperty('error')
    })

    it('should return 201 when registration was successful', async () => {
      const newBook = bookFactory()
      const { status, body } = await $http.post(endpoint).send(newBook)

      expect(status).toBe(201)
      expect(body).toHaveProperty('_id')
      expect(body).toEqual(expect.objectContaining(newBook))
    })
  })

  describe(`PUT ${endpoint}/:bookId - Update a book`, () => {
    let book = { _id: '' }
    const props = ['title', 'publisher']
    const randomIndex = faker.random.number({ min: 0, max: props.length - 1 })
    const updatedValue = faker.lorem.words()
    const updatedProp = props[randomIndex]

    beforeAll(async () => {
      book = (await Mongoose.model('Book')
        .findOne({})
        .lean()) || { _id: '' }
    })

    it('should return 404 when book with provided ID not found', async () => {
      const fakeId = Mongoose.Types.ObjectId()
      const { status, body } = await $http
        .put(`${endpoint}/${fakeId}`)
        .send({ ...book, [updatedProp]: updatedValue })

      expect(status).toBe(404)
      expect(body).toHaveProperty('error')
    })

    it('should return 400 when receives a invalid payload', async () => {
      const { _id } = book
      const { status, body } = await $http
        .put(`${endpoint}/${_id}`)
        .send({ ...book, [updatedProp]: '' })

      expect(status).toBe(400)
      expect(body).toHaveProperty('error')
    })

    it('should return 200 when book successful update', async () => {
      const { _id } = book
      const { status, body } = await $http
        .put(`${endpoint}/${_id}`)
        .send({ ...book, [updatedProp]: updatedValue })

      expect(status).toBe(200)
      expect(body).toEqual(
        expect.objectContaining({ [updatedProp]: updatedValue })
      )
    })
  })

  describe(`DELETE ${endpoint}/:bookId - Remove a book`, () => {
    it('should return 404 when book with provided ID not found', async () => {
      const fakeId = Mongoose.Types.ObjectId()
      const { status, body } = await $http.delete(`${endpoint}/${fakeId}`)
      expect(status).toBe(404)
      expect(body).toHaveProperty('error')
    })

    it('should return 200 when successful remove a book', async () => {
      const book = (await Mongoose.model('Book')
        .findOne({})
        .lean()) || { _id: '' }
      const _id = book._id.toString()
      const { status, body } = await $http.delete(`${endpoint}/${_id}`)

      expect(status).toBe(200)
      expect(body).toEqual(expect.objectContaining({ _id }))
    })
  })
})
