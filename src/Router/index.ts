import { Router as ExpressRouter } from 'express'

import { Books } from '@controllers'

const Router = ExpressRouter()

const BooksRouter = ExpressRouter()
BooksRouter.get('/', Books().list)
BooksRouter.get('/:bookId', Books().get)
BooksRouter.post('/', Books().create)
BooksRouter.put('/:bookId', Books().update)
BooksRouter.delete('/:bookId', Books().remove)

Router.use('/books', BooksRouter)

export default Router
