import { formatRelative } from 'date-fns'
import { ptBR } from 'date-fns/locale'
import { Response, Request } from 'express'
import * as Yup from 'yup'
import { ObjectSchema } from 'yup'

import { Book } from '@models'
import { $response } from '@utils'

const PayloadSchema: ObjectSchema<object> = Yup.object().shape({
  authors: Yup.array()
    .of(Yup.string())
    .min(1),
  categories: Yup.array()
    .of(Yup.string())
    .min(1),
  publisher: Yup.string().required(),
  rating: Yup.number(),
  title: Yup.string().required()
})

export default ({
  Model = Book,
  schema = PayloadSchema,
  $res = $response
} = {}) => {
  async function list(_: Request, res: Response) {
    const books = await Model.find()
    if (books.length > 0) {
      const payload = books.map(
        ({ _id, authors, title, publisher, rating, updatedAt }) => ({
          _id,
          title,
          publisher,
          rating,
          authors: authors.join(', '),
          updatedAt: formatRelative(updatedAt, new Date(), {
            locale: ptBR
          })
        })
      )
      return $res.success(res, payload)
    }
    return $res.notFound(res, { error: 'Nothing found here' })
  }

  async function get(req: Request, res: Response) {
    const { bookId } = req.params
    const book = await Model.findOne({ _id: bookId }).lean()

    if (book) {
      return $res.success(res, {
        ...book,
        updatedAt: formatRelative(book.updatedAt || Date.now(), new Date(), {
          locale: ptBR
        }),
        createdAt: formatRelative(book.createdAt || Date.now(), new Date(), {
          locale: ptBR
        })
      })
    }
    return $res.notFound(res, { error: 'Book with provided ID not found' })
  }

  async function create(req: Request, res: Response) {
    const { body: payload } = req

    const isValidPayload = await schema.isValid(payload)
    if (!isValidPayload) {
      return $res.badRequest(res, {
        error: 'Validation fails. Invalid payload.'
      })
    }

    const created = await Model.create(payload)
    return $res.created(res, created)
  }

  async function update(req: Request, res: Response) {
    const { bookId } = req.params
    const { body: payload } = req

    const isValidPayload = await schema.isValid(payload)
    if (!isValidPayload) {
      return $res.badRequest(res, {
        error: 'Invalid payload. Validation fails.'
      })
    }

    const found = await Model.findOne({ _id: bookId })
    if (!found) {
      return $res.notFound(res, { error: 'Book with provided ID not found' })
    }

    const updated =
      (await Model.findOneAndUpdate(
        { _id: bookId },
        { $set: payload },
        { new: true }
      )) || {}
    return $res.success(res, updated)
  }

  async function remove(req: Request, res: Response) {
    const { bookId } = req.params

    const found = await Model.findOne({ _id: bookId })
    if (!found) {
      return $res.notFound(res, { error: 'Book with provided ID not found' })
    }

    await Model.findOneAndRemove({ _id: bookId })
    return $res.success(res, { _id: bookId })
  }

  return { list, get, create, update, remove }
}
