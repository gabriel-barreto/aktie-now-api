import http from 'http'

import App from './app'

import { api as config } from './Configs'

const app = App()
http.createServer(app.app).listen(config.port, app.listen)
