// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path')

module.exports = {
  clearMocks: true,
  coverageDirectory: 'coverage',
  preset: 'ts-jest',
  testEnvironment: 'node',
  transform: { '^.+\\.tsx?$': 'ts-jest' },
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.ts$',
  testPathIgnorePatterns: ['/node_modules/'],
  moduleFileExtensions: ['ts', 'js', 'json', 'node'],
  moduleNameMapper: {
    '@config': path.resolve(__dirname, 'src', 'Configs', 'index.ts'),
    '@controllers': path.resolve(__dirname, 'src', 'Controllers', 'index.ts'),
    '@models': path.resolve(__dirname, 'src', 'Models', 'index.ts'),
    '@modules': path.resolve(__dirname, 'src', 'Modules', 'index.ts'),
    '@router': path.resolve(__dirname, 'src', 'Router', 'index.ts'),
    '@utils': path.resolve(__dirname, 'src', 'Utils', 'index.ts')
  }
}
